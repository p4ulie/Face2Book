#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
"""
Convert exported Facebook Timeline to book
"""

# from datetime import datetime
import dateutil.parser
from BeautifulSoup import BeautifulSoup
from HTMLParser import HTMLParser

FILENAME = 'data1/html/timeline.htm'
NAME = u'Pavol Antalík'

if __name__ == '__main__':
    data = open(FILENAME, 'r').read()

    soup = BeautifulSoup(data)
    contents = soup.body.find("div", {"class": "contents"})
    posts = [post(text=True) for post in list(reversed(contents.findAll("p")))]

    h = HTMLParser()

    for post in posts:
        if len(post) > 1:
            time_obj = dateutil.parser.parse(post[0])
            # time = datetime.strptime(post[0], '%A, %B %d, %Y at %I:%M%p %Z%z')
            text = h.unescape(post[1])

            actions = [NAME + u' shared ',
                       NAME + u' likes ',
                       NAME + u' went ',
                       NAME + u' was added to ',
                       NAME + u' followed ',
                       NAME + u' updated his status.',
                       NAME + u' in ',
                       u' are now friends.',
                       u' to movies he\'s watched.',
                       u' added a new photo.',
                       u' added a new photo to the album:',
                       u' changed his phone number to ',
                       u' published a note.',
                       u' joined ']

            filter_flag = False
            for action in actions:
                if action in text:
                    filter_flag = True
            if not filter_flag:
                print "%s: %s" % (time_obj, text)
